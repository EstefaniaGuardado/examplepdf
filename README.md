# Simple Example FPDF & FPDI #

FPDF is a PHP class which allows to generate PDF files with pure PHP.

FPDI class is an extension for FPDF allowing you to import existing PDF pages into FPDF.

This is a sample example using FPDF and FPDI with PHP and get like result a PDF. 

(It's a previous test for a project)